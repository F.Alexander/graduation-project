

import requests
from bs4 import BeautifulSoup as BS
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from cloudipsp import Api, Checkout
from catalog.forms import ReviewsForm, ProfileForm1, ProfileForm2
from catalog.models import Product, Basket, ProductInBasket, Reviews, LikeDislike, Order, ProductInOrder, Category, \
    Profile, Consist
from django.template.loader import render_to_string
from .forms import UserRegistrationForm, UserEditForm, ProfileEditForm
from time import sleep


def order_notify(request, url):
    user = User.objects.get(username=request.user.username)
    order = Order.objects.filter(user=user).last()
    in_order = ProductInOrder.objects.filter(order=order.id)
    context = {'basket_price': url}
    message = render_to_string('catalog/letter.html', context)
    user.email_user('NEW ORDER', message=message, from_email='piotrsiamuk@gmail.com')
    return redirect('catalog_list')


def catalog_list(request, *args, **kwargs):
    categories = Category.objects.all()
    consisties = Consist.objects.all()

    pk = kwargs.get('pk')
    if pk is None:
        products = Product.objects.all()
    else:
        products = Product.objects.filter(category=pk)

    if request.method == 'POST':
        ingr = request.POST.getlist('ingr')
        if ingr:
            consisties = Consist.objects.filter(pk__in=ingr)
            products = Product.objects.filter(consist__in=ingr).distinct()

    return render(request, 'catalog/catalog_list.html',
                  {'products': products, 'categories': categories, 'consisties': consisties})


def product_detail(request, pk):
    product = get_object_or_404(Product, pk=pk)
    consisties = product.consist.all()
    reviews = Reviews.objects.filter(product=pk).order_by('-like')
    if request.method == 'GET':
        form = ReviewsForm()
    elif request.method == 'POST':
        form = ReviewsForm(request.POST)
        if form.is_valid():
            new_review = form.save(commit=False)
            new_review.post = form
            new_review.username_id = request.user.id
            new_review.product_id = pk
            new_review.save()
            form = ReviewsForm()
    return render(request, 'catalog/product_detail.html',
                  {'product': product, 'form': form, 'reviews': reviews, 'consisties': consisties})


@login_required(login_url='login')
def review_like(request, pk, like):
    user = request.user
    review = Reviews.objects.get(pk=pk)
    LikeDislike.objects.get_or_create(review=review, user=request.user)
    if like == "like":
        LikeDislike.objects.filter(review=pk).update(like=True)
    elif like == "dislike":
        LikeDislike.objects.filter(user=user, review=pk).update(like=False)
    else:
        return redirect('product_detail', pk=review.product.pk)
    amount_like = len(LikeDislike.objects.filter(review=review.pk, like=True))
    amount_dislike = len(LikeDislike.objects.filter(review=review.pk, like=False))
    Reviews.objects.filter(pk=review.pk).update(like=amount_like, dislike=amount_dislike)

    return redirect('product_detail', pk=review.product.pk)


@login_required(login_url='login')
def add_in_basket(request, pk, value):
    basket_ = request.user.basket
    product = Product.objects.get(pk=pk)
    if not ProductInBasket.objects.filter(basket=basket_, product=product).exists():
        ProductInBasket.objects.create(basket=basket_, product=product)
    product = ProductInBasket.objects.get(basket=basket_, product=pk)
    if value == '+1':
        product.amount += 1
    elif value == '-1' and product.amount != 0:
        product.amount -= 1
    product.save()
    return redirect('product_detail', pk=pk)


@login_required(login_url='login')
def basket(request):
    user = request.user
    in_user_basket = ProductInBasket.objects.filter(basket=user.basket)
    for item in in_user_basket:
        if item.amount == 0:
            item.delete()
        else:
            item.amount_price = item.product.price * item.amount
            item.amount_priceUSD = item.product.price_usd * item.amount
            item.save()
    basket_price = request.user.basket.basket_price
    return render(request, 'catalog/basket.html',
                  {'in_user_basket': in_user_basket, 'basket_price': float(basket_price[0]),
                   "basket_price_usd": float(basket_price[1])})


def clear_basket(request):
    user = request.user
    Basket.clear_user_basket(user=user)
    return redirect('basket')


def clear_order(request):
    user = request.user
    try:
        Order.clear_order(user=user)
    except AttributeError:
        pass
    finally:
        return redirect('basket')


def create_order(request):
    user = request.user
    basket_price = user.basket.basket_price
    if not Order.objects.filter(user=user).exclude(confirm=True).exists():
        Order.objects.create(user=user)
    order_object = ProductInBasket.objects.filter(basket=user.basket)
    order = Order.objects.filter(user=user).last()
    order.order_price = basket_price[0]
    order.order_priceUSD = basket_price[1]
    order.save()
    for object in order_object:
        rows = ProductInOrder.objects.filter(order=order, product=object.product)
        if rows.exists():
            rows.update(amount=object.amount, amount_price=object.amount_price,
                        amount_priceUSD=object.amount_priceUSD)
        else:
            ProductInOrder.objects.create(order=order, amount=object.amount, product=object.product,
                                          amount_price=object.amount_price,
                                          amount_priceUSD=object.amount_priceUSD)
    in_order = ProductInOrder.objects.filter(order=order.id)
    return render(request, 'catalog/order.html',
                  {'in_order': in_order, 'basket_price': float(basket_price[0]),
                   "basket_price_usd": float(basket_price[1])})


def orders_history(request):
    user = request.user
    user_orders_history = Order.objects.filter(user=user)
    return render(request, 'catalog/User orders history.html', {'user_orders_history': user_orders_history})


def confirm_answer(request):
    user = request.user
    user.user_order.filter(user=user.pk).update(confirm=True)
    clear_basket(request)
    clear_order(request)
    return redirect('catalog_list')


class login(LoginView):
    form = AuthenticationForm()
    template_name = 'catalog/login.html'

    def get_success_url(self):
        return reverse_lazy('catalog_list')


def logout(request):
    auth_logout(request)
    return redirect("login")


def parser():
    r = requests.get('https://www.nbrb.by/statistics/rates/ratesdaily.asp')
    html = BS(r.content, 'html.parser')
    usd = html.select('.curCours > div')
    item = usd[5]
    currency = item.get_text()
    result = ''
    for i in currency:
        if i == ',':
            i = '.'
        result += i
    return float(result)


def price_converter():
    products = Product.objects.all()
    for items in products:
        items.price_usd = float(items.price) / parser()
        Product.objects.filter(pk=items.id).update(price_usd=items.price_usd)


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            Profile.objects.create(user=new_user)
            Basket.objects.create(user=new_user)
            return render(request, 'catalog/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'catalog/registration.html', {'user_form': user_form})


@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user, data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile, data=request.POST, files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
        return redirect('profile', pk=request.user.pk)
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
        return render(request,
                      'catalog/edit.html',
                      {'user_form': user_form,
                       'profile_form': profile_form})


def profile_detail(request, pk):
    detail1 = ProfileForm1(instance=request.user)
    detail2 = ProfileForm2(instance=request.user.profile)
    return render(request,
                  'catalog/profile_detail.html',
                  {'det1': detail1,
                   'det2': detail2})


def delay():
    sleep(20)
    return redirect('catalog_list')


def buy(request):
    user = request.user
    order = Order.objects.filter(user=user).last()
    amount_ = str(float(order.order_price))
    x = amount_.split('.')
    y = x[0] + x[1]
    api = Api(merchant_id=1397120,
              secret_key='test')
    checkout = Checkout(api=api)
    data = {
        "currency": "BYN",
        "amount": int(y),
    }
    url = checkout.url(data).get('checkout_url')
    order_notify(request, url)
    confirm_answer(request)
    return redirect('catalog_list')
