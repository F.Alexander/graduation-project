from django.urls import path
from catalog.views import *

urlpatterns = [
    path('catalog_list/', catalog_list, name="catalog_list"),
    path('catalog_list/<int:pk>/', catalog_list, name="catalog_list"),
    path('basket/', basket, name="basket"),
    path('add_in_basket/<int:pk><str:value>/', add_in_basket, name="add_in_basket"),
    path('clear_basket/', clear_basket, name="clear_basket"),
    path('clear_ordere/', clear_order, name="clear_order"),
    path('product_detail/<int:pk>/', product_detail, name="product_detail"),
    path('review_like/<int:pk><str:like>/', review_like, name="review_like"),
    path('registration/', register, name="registration"),
    path('login/', login.as_view(), name="login"),
    path('logout/', logout, name="logout"),
    path('create_order/', create_order, name="create_order"),
    path('profile_detail/<int:pk>/', profile_detail, name="profile"),
    path('orders_history/', orders_history, name="orders_history"),
    path('edit/', edit, name='edit'),
    path('buy/', buy, name='buy'),
    path('order_notify/', order_notify, name='order_notify'),

]
