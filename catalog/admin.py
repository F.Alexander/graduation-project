from django.contrib import admin
from catalog.models import *


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'email', 'first_name', 'last_name', 'address', 'phone', 'image']


# Register your models here.
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Basket)
admin.site.register(ProductInBasket)
admin.site.register(Reviews)
admin.site.register(LikeDislike)
admin.site.register(Order)
admin.site.register(ProductInOrder)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Consist)

