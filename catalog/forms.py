from django import forms
from django.contrib.auth.models import User

from catalog.models import Reviews, ProductInOrder, Profile


class ReviewsForm(forms.ModelForm):
    class Meta:
        model = Reviews
        fields = ('text',)


class ContactForm(forms.ModelForm):
    subject = forms.CharField(label='Confirmation order')

    class Meta:
        model = ProductInOrder
        fields = (
            'product',
            'amount_price',
        )


from .models import Profile


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('address', 'phone', 'image')


class ProfileForm1(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',

        )


class ProfileForm2(forms.ModelForm):
    class Meta:
        model = Profile
        fields = (
            'address',
            'phone',
            'image',
        )
