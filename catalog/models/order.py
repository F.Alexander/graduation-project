from django.contrib.auth.models import User
from django.db import models
from .product import Product
from decimal import Decimal


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_order')
    date_order = models.DateTimeField(auto_now_add=True)
    confirm = models.BooleanField(default=False)
    order_price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))
    order_priceUSD = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))

    def __str__(self):
        return f"{self.user, self.confirm, self.order_price, self.order_priceUSD, self.date_order}"

    @classmethod
    def clear_order(cls, user):
        Order.objects.filter(user=user).exclude(confirm=True).last().delete()


class ProductInOrder(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='in_order')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    amount_price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))
    amount_priceUSD = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))

    def __str__(self):
        return f"{self.product, self.amount, self.amount_price, self.amount_priceUSD}"
