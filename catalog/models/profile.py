from django.contrib.auth.models import User
from django.db import models
from django.conf import settings


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    email = models.EmailField(max_length=50, default='')
    first_name = models.CharField(max_length=50, default="")
    last_name = models.CharField(max_length=50, default="")
    address = models.CharField(max_length=50, default="")
    phone = models.IntegerField(default='+375000000')
    image = models.ImageField(upload_to='image_profile', blank=True, default='/image_profile/alen.jpg')

    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)
