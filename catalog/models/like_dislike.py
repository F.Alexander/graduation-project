from django.db import models
from django.contrib.auth.models import User
from .reviews import Reviews


class LikeDislike(models.Model):
    review = models.ForeignKey(Reviews, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    like = models.BooleanField(null=True)

    def __str__(self):
        return f"{self.review} | {self.user} | {self.like}"
