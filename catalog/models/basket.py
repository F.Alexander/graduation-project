from django.contrib.auth.models import User
from django.db import models
from .product import Product
from decimal import Decimal


class Basket(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='basket')

    def __str__(self):
        return f'{self.user}'

    @property
    def basket_price(self):
        basket_price = 0
        basket_price_usd = 0
        basket_product = self.products.all()
        for el in basket_product:
            basket_price += el.amount_price
            basket_price_usd += el.amount_priceUSD
        return [basket_price, basket_price_usd]

    @classmethod
    def clear_user_basket(cls, user):
        return user.basket.products.all().delete()


class ProductInBasket(models.Model):
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE, related_name='products')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    amount_price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))
    amount_priceUSD = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))

    def __str__(self):
        return f'{self.basket}'
