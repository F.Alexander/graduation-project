from django.db import models
from decimal import Decimal



class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))
    price_usd = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal("0.00"))
    description = models.TextField(max_length=300)
    consist = models.ManyToManyField('Consist', related_name='consist_ingridient')
    category = models.ForeignKey('Category', on_delete=models.CASCADE, related_name='category_product')
    image = models.ImageField(upload_to='image_product', blank=True)

    def __str__(self):
        return f"{self.name}"
