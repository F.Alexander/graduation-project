from django.db import models

class Consist(models.Model):
    ingridient = models.TextField(max_length=300)

    def __str__(self):
        return f"{self.ingridient}"